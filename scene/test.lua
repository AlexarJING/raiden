local scene = gamestate.new()

game = require "scr/game"
editor = require "scr/editor"

function scene:enter()
	game:init()
	editor:init()
end

function scene:leave()

end

function scene:update(dt)
	game:update(dt)
end


function scene:draw()
	game:draw()
end
return scene