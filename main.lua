require "lib/autobatch"
require "lib/util"
bump=require "lib/bump"
class=require "lib/middleclass"
gamestate= require "lib/hump/gamestate"
Tween= require "lib/tween"
delay= require "lib/delay"
Animation = require "lib/animation"
playSound = require "lib/sound"
res = require "scr.resloader"
require "lib/gooi"
require "lib/autobatch"
--DEBUG = true
function love.load()
    love.graphics.setFont(res.font)
    gameState={}
    for _,name in ipairs(love.filesystem.getDirectoryItems("scene")) do
        gameState[name:sub(1,-5)]=require("scene."..name:sub(1,-5))
    end
    gamestate.registerEvents()
    gamestate.switch(gameState.test)
end

function love.update(dt) delay:update(dt) end
function love.mousereleased(x, y, button) gooi.released() end
function love.mousepressed(x, y, button)  gooi.pressed() end
function love.textinput(text) gooi.textinput(text) end
function love.keypressed(key) gooi.keypressed(key) end

