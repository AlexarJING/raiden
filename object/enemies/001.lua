local base = require "object/enemy"
local ship = class("001",base)
local Bullet = require "object/bullet"
local myBullet = class("001",Bullet)
myBullet.speed = 200
myBullet.color = {255,love.math.random(50,255),love.math.random(50,255)}
myBullet.fireCD = 1.5
myBullet.size = 20
myBullet.damage = 5

ship.weaponData = {
    {0,-8,0.1,myBullet},
    {0,-8,-0.1,myBullet},
}
ship.engineData = {
    {0,8,0,1}
}
ship.type = 1
ship.speed = 150
ship.hpMax = 200
ship.size = 4 
ship.droprate = 0.1
ship.moveTimer= 0

function ship:behavior()
    if not self.state then --初始化
        local tween = Tween.new(
            2,self,{y = love.math.random(50,300)},"inQuad",function() self.state = "ok" end)
        self:addTween(tween)
        self.state = "wait"
    elseif self.state == "ok" then
        self.moveTimer = self.moveTimer + 0.05
        self.x = self.x + math.sin(self.moveTimer)*1.5 
        self:fire()    
    end
end

return ship