local base = require "object/enemy"
local ship = class("001",base)
local Bullet = require "object/bullet"
local myBullet = class("001",Bullet)
myBullet.speed = 200
myBullet.color = {love.math.random(50,255),love.math.random(50,255),255}
myBullet.fireCD = 1
myBullet.size = 20
myBullet.damage = 8

ship.weaponData = {
    {0,-8,0.1,myBullet},
}

ship.engineData = {
    {0,8,0,3}
}
ship.type = 1
ship.speed = 150
ship.hpMax = 100
ship.droprate = 0.05

local t = 0
function ship:behavior()
    if not self.state then --初始化
        local tween = Tween.new(2,self,{y = love.math.random(50,300)},"inQuad",
            function() self.state = "ok" end)
        self:addTween(tween)
        self.state = "wait"
    elseif self.state == "ok" then
        self.rot = math.getRot(self.x,self.y,game.player.x,game.player.y)
        self:fire()    
    end
end

return ship