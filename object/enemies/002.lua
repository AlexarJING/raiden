local base = require "object/enemy"
local ship = class("001",base)
local Bullet = require "object/bullet"
local myBullet = class("001",Bullet)
myBullet.speed = 200
myBullet.color = {love.math.random(50,255),love.math.random(50,255),255}
myBullet.fireCD = 1
myBullet.size = 20
myBullet.damage = 10

ship.weaponData = {
    {0,-8,0,myBullet}
}

ship.engineData = {
    {0,8,0,2}
}
ship.speed = 150
ship.type = 2
ship.hp = 30
ship.size = 2 

function ship:behavior()
    if not self.state then --初始化
        self.vy = 200
        self.vx = 0
        self.state = "wait"
    else
        self.vy = self.vy+5
    end
end

return ship