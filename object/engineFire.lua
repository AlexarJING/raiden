local base = require "object/base"
local fire = class("engineFire")

function fire:init(parent, x, y, rot, type)
    self.parent = parent
    self.offx = x or 0
    self.offy = y or 8 
    self.rot = rot or 0
    self.type = type or love.math.random(1,4)
    self.color = {255,255,255}	
    self.anim = res.engineFire[self.type]:copy()
    self.anim.currentFrame=love.math.random(1,self.anim.maxFrame)
end

function fire:update(dt)
    self.anim:update(dt)
    if math.abs(self.parent.dx)>0 then
        self.anim.maxFrame=4
    else
        self.anim.maxFrame=2
    end
end

function fire:draw()
    love.graphics.setColor(self.color)
    local offx,offy=math.axisRot(self.offx*self.parent.scaleX,self.offy*self.parent.scaleY,
        self.parent.rot )
    self.anim:draw(self.parent.x+offx, self.parent.y+offy, self.parent.rot+self.rot -math.pi/2,
        self.parent.scaleX, self.parent.scaleY, 0, 4)    
end

return fire