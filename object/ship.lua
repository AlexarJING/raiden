local collidable = require "object/collidable"
local ship = class("ship_base",collidable)
local EngineFire = require "object/engineFire"
local Bullet = require "object/bullet"
local Frag = require "object/frag"
local Boom = require "object/boom"
local Missile = require "object/missile"
ship.hpMax = 100
ship.speed = 300

function ship:init(...)
    collidable.init(self,...)
    self.hp = self.hpMax
    self.image = res.sheet 
    self.quad = res.ships.blue[self.type] or res.ships.blue[1]
    self:initEngineSystem(self.engineData or {})
    self:initFireSystem(self.weaponData or {})
end

function ship:newWeapon(offx,offy,rot,bullet)
    local weapon = {
        offx = offx or 0,
        offy = offy or -8,
        rot = rot or 0,
        bullet = bullet,
        timer = 0,
        cd = bullet.fireCD
    }
    table.insert(self.weapons,weapon)
    return weapon
end

function ship:initFireSystem(data)
    self.weapons = {}    
    for i,v in ipairs(data) do        
        self:newWeapon(unpack(v))
    end
    
end

function ship:initEngineSystem(data)
    self.fireAnims = {}
    for i,v in ipairs(data) do
        local engine = EngineFire(self,unpack(v))
        table.insert(self.fireAnims,engine)
    end
    EngineFire(self)
end


function ship:weaponUpdate(dt)
    for _,weapon in ipairs(self.weapons) do
        weapon.timer = weapon.timer - dt
    end
end

function ship:fire(gun,offx,offy,rot)
    if gun then
        gun(self,offx,offy,rot)
        return
    end
    for _,weapon in ipairs(self.weapons) do
        if weapon.timer < 0 then
            weapon.bullet(self,weapon.offx,weapon.offy,weapon.rot)
            weapon.timer = weapon.cd
        end
    end
end

function ship:damage(p)
    if self.overwhelming then return end
    self.hp = self.hp - p
    if self.hp<0 then
       self:destroy() 
    end
end

function ship:destroy()
    collidable.destroy(self) 
    Boom(self,self.scaleX*2)
    Frag(self)
end

function ship:update(dt)
    collidable.update(self,dt)
    for _,fire in ipairs(self.fireAnims) do
        fire:update(dt)
    end
    self:weaponUpdate(dt)
    if self.behavior then
        self:behavior()
    end
end


function ship:draw()
    collidable.draw(self)
    if self.overwhelming then
       love.graphics.setColor(79, 255, 255,100)
       love.graphics.circle("fill",self.x,self.y,10*self.size)
    end
    for _,fire in ipairs(self.fireAnims) do
        fire:draw()
    end
end

return ship