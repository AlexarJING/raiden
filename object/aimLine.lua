local base = require "object/base"
local line = class("line",base)
line.color = {255,0,0} 

function line:init(parent,aim,lasting,...)
    base.init(self,... )
    self.parent = parent
  	self.aim = aim
  	self.x = parent.x
  	self.y = parent.y
  	self.rot = math.getRot(aim.x,aim.y,parent.x,parent.y)
  	self.lasting = lasting or 3
end

function line:update(dt)
    base.update(self,dt)
    self.lasting = self.lasting - dt
    if self.parent.destroyed or not self.aim or self.lasting<0 then 
    	self:destroy()
    end
    self.x = self.parent.x
    self.y = self.parent.y
    self.rot = math.getRot(self.aim.x,self.aim.y,self.parent.x,self.parent.y)
end

function line:draw()
	love.graphics.setColor(self.color)
	love.graphics.setColor(255, 0, 0) 
    love.graphics.line(
        -1000*math.sin(self.rot)+self.x,
        1000*math.cos(self.rot)+self.y+6*self.parent.size,
        self.x,self.y+6*self.parent.size)
end

return line