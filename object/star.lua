local base = require "object/base"
local star = class("star",base)

function star:init(...)
    base.init(self,... )
    self.type = love.math.random(1,6)
    self.w = 8
    self.h = 8
    self.size = love.math.random()+0.5
    self.scaleX = self.size
    self.scaleY = self.size
    self.anim = res.stars[self.type]:copy()
    self.anim.currentFrame=love.math.random(1,self.anim.maxFrame)
    self.vy = 30
end

function star:reset()
    self.x = love.math.random(0,500)
    self.y = -10
    self.type = love.math.random(1,6)
    self.w = 8
    self.h = 8
    self.size = love.math.random()+0.5
    self.scaleX = self.size
    self.scaleY = self.size
    self.anim = res.stars[self.type]:copy()
    self.anim.currentFrame=love.math.random(1,self.anim.maxFrame)
    self.vy = 30
    
end

function star:onDestroy()
   --star(love.math.random(0,500),-10) 
end

function star:update(dt)
    base.update(self,dt)
    if self.y > 800 then self:reset() end
end

return star