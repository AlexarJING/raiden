local ship = require "object/ship"
local player = class("player",ship)
local Bullet = require "object/bullet"
local Missile = require "object/missile"
Bullet.damage= 30
player.weaponData = {
    {0,-8,0,Bullet},
    --{2,-8,0,Bullet},
    --{0,-8,0,Missile}
}

player.engineData = {
    {0,8,0,3}
}

player.upgradeLevels = {
    front = 1,
    around = 0,
    missile = 0,
    speed = 0
}

player.type = 4
player.hpMax = 150

function player:init(...)
    ship.init(self,...) 
    self.tag = "player"
    game.player = self
    self.credit = 3
end


function player:onDestroy()
    delay:new(2,function() game:gameover() end)
end

function player:keyControl()
    local down = love.keyboard.isDown
    local dt = love.timer.getDelta()
    if down("w") then
       self.y = self.y - self.speed * dt
    elseif down("s") then
        self.y = self.y + self.speed * dt
    end
    if down("a") then
        self.x = self.x - self.speed * dt
    elseif down("d") then
        self.x = self.x + self.speed * dt
    end
    
    if down("space") then
       self:fire()
    end
 end

function player:limit()
    if self.x<16 then 
        self.x = 16 
    elseif self.x>484 then 
        self.x = 484
    end
    
    if self.y<16 then
        self.y = 16
    elseif self.y>784 then
        self.y = 784
    end
end

function player:collision(cols)
    for i,col in ipairs(cols) do
        local other = col.other
		if other.tag == "enemy" then 
            self:damage(other.hp)
            other:destroy()
        elseif other.tag == "item" then
            self:getItem(other)
            other:destroy()
        end
    end
end

function player:getItem(item)
    item.onCollision(self)
    self:upgrade()
end

function player:upgrade()
    local data = {}
    self.speed = player.speed + self.upgradeLevels.speed*20
    local front = self.upgradeLevels.front
    for i = 1,front do
        local offx = -(front+1)*2.5 + i*5
        table.insert(data,{offx,-8,0,Bullet})
    end
    
    local around = self.upgradeLevels.around
    for i = 1,around do
        local rot = -(around+1)*0.5*0.3 + i*0.3
        table.insert(data,{0,-8,rot,Bullet})
    end
    
    local missile = self.upgradeLevels.missile
    for i = 1,missile do
        local rot = love.math.random()*2*math.pi
        table.insert(data,{0,-8,rot,Missile})
    end
    self.weaponData = data
    self:initFireSystem(self.weaponData)
end

function player:update(dt)
    self:keyControl()
    self:limit()
    ship.update(self,dt)
end

return player