local Base = class("base")
Base.color = {255,255,255}
Base.size = 3
function Base:init(x,y,rot,w,h,size,...)
    self.x = x or 400
    self.y = y or 300
    self.ox = self.x
    self.oy = self.y
    self.dx = 0
    self.dy = 0
    self.vx = self.vx or 0
    self.vy = self.vx or 0
    self.scaleX = self.size or size or 3
    self.scaleY = self.size or size or 3
    self.w = w or 16
    self.h = h or 16
    self.anchorX = self.w/2
    self.anchorY = self.h/2
    self.rot  = self.rot or rot or 0
    self.tweens = {}
    table.insert(game.objects,self)
    self:onCreate()
end

function Base:update(dt)
    if self.destroyed then return end
    self:translate(dt)
    if self.anim then self.anim:update(dt) end
    if self.tweens then
        for i,tween in pairs(self.tweens) do
            tween:update(dt)
        end
    end
end


function Base:draw()
    love.graphics.setColor(self.color)
    if self.anim then 
        self.anim:draw(self.x,self.y,self.rot,self.scaleX,self.scaleY,self.anchorX,self.anchorY) 
    end
    
    if self.image then
        if self.quad then
            love.graphics.draw(self.image,self.quad,self.x,self.y,self.rot - math.pi/2,
                self.scaleX,self.scaleY,self.anchorX,self.anchorY)
        else
            love.graphics.draw(self.image,self.x,self.y,self.rot - math.pi/2,
                self.scaleX,self.scaleY,self.anchorX,self.anchorY)
        end
    end
    
    if DUBUG then
        love.graphics.setColor(255, 0, 128)
        love.graphics.circle(self.x,self.y,5)
    end
end

function Base:translate(dt)
    self.x = self.x + self.vx * dt
    self.y = self.y + self.vy * dt
    self.dx = self.x - self.ox
    self.dy = self.y - self.oy
    self.ox = self.x
    self.oy = self.y
end

function Base:destroy()
    self.destroyed = true 
    self:onDestroy()
end


function Base:onCreate()
    
end

function Base:onDestroy()
    
end

function Base:addTween(t)
    if t.callback then
        local cb = t.callback
        t.callback = function()
            table.removeItem(self.tweens,t)
            cb()
        end
    else
        t.callback = function()
            table.removeItem(self.tweens,t)
        end
    end
    table.insert(self.tweens,t)
end

return Base