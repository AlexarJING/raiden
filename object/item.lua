local ship = require "object/ship"
local item = class("item",ship)
item.tag = "item"

local function randomMove(self)
    if not self.tweens[1] and not laserTarget then
            self:addTween(
                Tween.new(2,self,
                    {x = love.math.random(100,400)},
                    "inOutQuad")
            )
        end
end

local colors = {"blue","green","red","yellow","purple"}
local cbs = {
    function(self) self.upgradeLevels.speed = self.upgradeLevels.speed + 1 end,
    function(self) self.hp = self.hp + 30; if self.hp>self.hpMax then self.hp = self.hpMax end end, 
    function(self) self.upgradeLevels.front = self.upgradeLevels.front + 1 end,
    function(self) self.upgradeLevels.around = self.upgradeLevels.around + 1 end,
    function(self) self.upgradeLevels.missile = self.upgradeLevels.missile + 1 end,
}


function item:init(parent,...)
    ship.init(self,...) 
    self.x = parent.x
    self.y = parent.y
    self.vy = 60
    self.itemIndex = love.math.random(1,5)
    self.quad = res.ships[colors[self.itemIndex]][61]
    self.onCollision = cbs[self.itemIndex]
end

function item:behavior()
   randomMove(self) 
end

return item