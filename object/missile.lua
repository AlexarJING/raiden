local bullet = require "object/bullet"
local missile = class("missile",bullet)
local EngineFire = require "object/engineFire"
missile.speed = 300
missile.dr = 0.05
missile.fireCD = 1
missile.damage = 50
missile.size = 2
missile.w = 16
missile.h = 16
function missile:init(...)
    bullet.init(self,...)
    self.image = res.sheet
    self.quad = res.missile
    self.engineFire = EngineFire(self,0,10,0,3)
    self.color = {255,255,255,255}
end

function missile:getAABB()
    local w = self.scaleX*self.w/2
    local h = self.scaleY*self.h/2
    local x = self.x - w/2
    local y = self.y - h/2
    return x,y,w,h
end

function missile:setPosition(l,t)
    self.x = l + self.scaleX*self.w/4
    self.y = t + self.scaleX*self.h/4
end

function missile:findTarget()
    if self.target and not self.target.destroyed then return end
    local target = game.world:getNearestItem(self,"enemy")
    self.target = target
    --target.color = {255,0,0}
end

function missile:traceTarget()
    if not self.target then return end
    local tx,ty = self.target.x,self.target.y
	local rot = math.unitAngle(math.getRot(self.x,self.y,tx,ty)) --这里是计算方位角的一种方法。
	self.rot = math.unitAngle(self.rot)
    if rot>self.rot and math.abs(rot - self.rot)< math.pi or
		 rot<self.rot and  math.abs(rot - self.rot)> math.pi then
		self.rot = self.rot + self.dr
	else
		self.rot = self.rot - self.dr
	end
    self.vx = self.speed*math.sin(self.rot)
	self.vy = -self.speed*math.cos(self.rot)
    
end

function missile:update(dt)
    self:findTarget()
    self:traceTarget()
    bullet.update(self,dt)
    self.engineFire:update(dt)
end

function missile:draw()
    bullet.draw(self)
    self.engineFire:draw()
end

return missile