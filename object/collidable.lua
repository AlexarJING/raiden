local base = require "object/base"
local collidable = class("collidable",base)

function collidable:init(...)
   base.init(self,...) 
   self:initBump()
end


function collidable:update(dt)
    base.update(self,dt)
    if self.destroyed then return end
    self:updateBump()
end


function collidable:draw()
    base.draw(self)
    if DEBUG then
		love.graphics.rectangle("line", self:getAABB())
	end
end

function collidable:getAABB()
    local w = self.scaleX*self.w/1.5
    local h = self.scaleY*self.h/1.5
    local x = self.x - w/2
    local y = self.y - h/2
    return x,y,w,h
end

function collidable:setPosition(l,t)
    self.x = l + self.scaleX*self.w/3
    self.y = t + self.scaleX*self.h/3
end

function collidable:initBump()
    game.world:add(self,self:getAABB())
end

function collidable:destroy()
    if not self.destroyed then
        game.world:remove(self)
    end
    base.destroy(self)
end

function collidable.collidefilter(me,other)
    return bump.Response_Cross
end

function collidable:collision(cols)
    
end


function collidable:updateBump()
    local ox,oy = self:getAABB()
	local tx,ty ,cols = game.world:move(self,ox,oy,self.collidefilter)
	self:setPosition(tx,ty)
	self:collision(cols)
end

return collidable