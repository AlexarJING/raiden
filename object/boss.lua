local enemy = require "object/enemy"
local boss = class("boss",enemy)

function boss:init(...)
    enemy.init(self,...)
    game.boss = self
end

function boss:destroy()
    enemy.destroy(self) 
    game.boss = nil
end

return boss