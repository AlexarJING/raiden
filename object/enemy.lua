local Ship = require "object/ship"
local enemy = class("enemy",Ship)
enemy.droprate = 0.1
local Item = require "object/item"

function enemy:init(...)
    Ship.init(self,...)
    self.tag = "enemy"
end

function enemy:behavior()
    if self.state ~= "ok" then
        self.vx = self.speed * math.sin(self.rot)
        self.vy = -self.speed * math.cos(self.rot)
        self.state = "ok"
    end
    self:fire()    
end

function enemy:outTest()
    if self.y>1000 or self.y<-1000 then self:destroy() end
    if self.x<-500 or self.x>1000 then self:destroy() end
end

function enemy:update(dt)
    self:outTest()
    if self.destroyed then return end
    Ship.update(self,dt)
end

function enemy:destroy()
    Ship.destroy(self)
    if love.math.random()<self.droprate then
        Item(self)
    end
end

return enemy