local base = require "object/boss"
local ship = class("001",base)
local Bullet = require "object/bullet"
local myBullet = class("shot",Bullet)
local Line = require "object/aimLine"
local Circle = require "object/target"


myBullet.speed = 200
myBullet.color = {255,100,100}
myBullet.fireCD = 1
myBullet.size = 20
myBullet.damage = 5

local Laser = class("laser",Bullet)
Laser.color = {100,200,255}
Laser.fireCD = 0.5
Laser.size = 20
Laser.speed = 300


local Missile = class("missile2",require "object/missile")
Missile.speed = 300
Missile.dr = 0.05
Missile.fireCD = 1
Missile.damage = 50
Missile.size = 2
function Missile:findTarget()
    if math.abs(self.x-self.target.x)<10 and 
        math.abs(self.y - self.target.y)<10 then
        self:destroy()
    end
end


ship.size = 7
ship.hpMax = 12000
ship.weaponData = {}
ship.engineData = {
    {-0.5,10.5,0,4}
}
ship.type = 73
ship.speed = 150
local overwhelmingDelay = 3

local shotCD = 2
local shotTimer = 2
local function shot(self,rate)
    local target = game.player
    local dt = love.timer.getDelta()
    shotTimer = shotTimer - dt
    if shotTimer < 0  then
        shotTimer = shotCD*(rate or 1)
        local target = game.player
        local rot = math.getRot(target.x,target.y,self.x,self.y)
        for i = -0.5,0.5,0.2 do
           self:fire(myBullet,0,-8,rot+i) 
        end
    end
end

local shot2CD = 5
local shot2Timer = 5
local shot2Count = 5
local function shot2(self,rate)
    local target = game.player
    local dt = love.timer.getDelta()
    shot2Timer = shot2Timer - dt
    if shot2Timer < 0 then
        shot2CD = shot2CD - 0.1
        shot2Timer = shot2CD*(rate or 1)
        local target = game.player
        local rot = math.getRot(target.x,target.y,self.x,self.y)
        self:fire(myBullet,0,-8,rot) 
        for i = 1,shot2Count do
            if target.dx == 0 then
                self:fire(myBullet,0,-8,rot+ (0.5-love.math.random())*0.3) 
            else
                self:fire(myBullet,0,-8,rot+love.math.random()*0.3*target.dx) 
            end
        end
    end
end

local laserCharge = 2
local laserChargeTimer = 2
local laserLast = 2
local laserLastTimer = 3
local laserCD = 3
local laserTimer = 0.1
local laserTarget = nil
local laserFireTimer = 0
local laserFireCD = 0.1
local bigLaserIndex = 1
local bigLaserCount = 5
local laserAimLine =nil
local laserFireRot = 0
local function laser(self,rate)
    local target = game.player
    local dt = love.timer.getDelta()
    laserTimer = laserTimer - dt
    if laserTimer < 0  then
        if not laserTarget then
            laserTarget = target
            laserAimLine = Line(self,laserTarget)
        else
            laserChargeTimer = laserChargeTimer - dt
            if laserChargeTimer < 0 then
                laserLastTimer = laserLastTimer - dt
                if laserAimLine then
                    laserFireRot = laserAimLine.rot
                    laserAimLine:destroy()
                    laserAimLine = nil                    
                end
                laserFireTimer = laserFireTimer - dt
                if laserFireTimer< 0 then
                    self:fire(Laser,0,-8,laserFireRot)
                    laserFireTimer = laserFireCD
                end
                if laserLastTimer < 0 then
                    if rate then 
                        bigLaserIndex = bigLaserIndex + 1
                        if bigLaserIndex> bigLaserCount then
                            laserLastTimer = laserLast
                        else
                            laserLastTimer = laserLast*rate
                        end
                    else
                       laserLastTimer = laserLast
                    end
                    laserTimer = laserCD 
                    laserChargeTimer = laserCharge
                    laserTarget = nil
                end
            end
        end   
    end
end


local missileCD = 3
local missileTimer = 0

local function missile(self,count,rate)
    local target = game.player
    local dt = love.timer.getDelta()
    missileTimer = missileTimer - dt
    if missileTimer<0 then
        for i = 1, count or 1 do
            local t = {
                x = target.x,
                y = target.y
            }
            self:fire(Missile,0,0,love.math.random()*Pi*2)
            local m = game.objects[#game.objects]
            m.target = t
            Circle(self,t)
        end
        missileTimer = missileCD*(rate or 1)
    end
end

local function randomMove(self,x,y)
    if not self.tweens[1] and not laserTarget then
            self:addTween(
                Tween.new(2,self,
                    {x = x or love.math.random(100,400),y = y or love.math.random(50,250)},
                    "inOutQuad")
            )
        end
end

function ship:behavior()
    if not self.state then --初始化
        local tween = Tween.new(2,self,{y = 150},"inQuad",function() self.state = "p1" end)
        self:addTween(tween)
        self.state = "wait"
    elseif self.state == "p1" then
        randomMove(self)
        shot(self)
        shot2(self)
        if self.hp <= self.hpMax*3/4 then
            self.state = "p1p2"
            self.overwhelming = true
            delay:new(10,function() 
                self.overwhelming = false 
                self.state = "p2"
            end)
            shotTimer = 0
        end
    elseif self.state == "p1p2" then
        shot(self,0.5)
        shot2(self)
        laser(self)
    elseif self.state == "p2" then
        randomMove(self)
        shot(self)
        shot2(self)
        laser(self,0.25)
        if self.hp <= self.hpMax*1/4 then
            self.state = "p2p3"
            self.overwhelming = true
            self:addTween(
                Tween.new(2,self,
                    {x = 250,y = 150},
                    "inOutQuad")
            )
            delay:new(5,function() 
                self.overwhelming = false 
                self.state = "p3"
            end)
            shotTimer = 0
        end
    elseif self.state == "p2p3" then
        shot(self,0.5)
        shot2(self)
        laser(self)
        missile(self,3)
    elseif self.state == "p3" then
        shot(self,0.5)
        shot2(self,0.5)
        laser(self,0.5)
        missile(self,3,0.5)
    end
end


return ship