local collidable = require "object/collidable"
local Boom = require "object/boom"
Bullet = class("bullet",collidable)
Bullet.fireCD = 0.1
Bullet.size = 10
Bullet.speed = 800
Bullet.damage = 15
Bullet.lifeMax = 10
Bullet.w = 1
Bullet.h = 1
Bullet.tag = "bullet"
Bullet.color = {255,255,0,255}

local circleVert = {}
circleVert[1] = {0,0,0.5,0.5,255,255,255}
for i = 0,30 do
    local rad = i*2*math.pi/30
    local x = math.sin(rad)
    local y = math.cos(rad)
    table.insert(circleVert,{x,y,(32+x*30)/64,(32+y*32)/64,255,255,255,100})
end
Bullet.circleMesh = love.graphics.newMesh(circleVert,"fan") --quest 2



function Bullet:init(parent,x,y,rot,...)
	local x = x or 0
    local y = y or -5
    local rot = parent.rot + (rot or 0)
    local size = self.size
    local offx,offy = math.axisRot(x,y,rot)
    collidable.init(self,parent.x+offx*parent.size,parent.y+offy*parent.size,
        rot,self.w,self.h,size,size)
    self.parent = parent
    self.vx = self.speed * math.sin(self.rot)
	self.vy = -self.speed * math.cos(self.rot)
    self.life = self.lifeMax
end

function Bullet:edge()
   if self.x<-200 or self.x>700 then self:destroy(true) end
   if self.y<0 or self.y>1000 then self:destroy(true) end
end

function Bullet:update(dt)
    collidable.update(self,dt) 
    self.life = self.life -dt
    self:edge()
    if self.life<0 then
        self:destroy()
    end
end

Bullet.canbeTarget = {
    bullet = false,
    player = true,
    enemy = true,
    item = false
}

function Bullet:draw()
    collidable.draw(self)
    if not self.image and not self.anim then 
        love.graphics.setColor(self.color)
        --love.graphics.circle("fill",self.x,self.y,self.size)
        love.graphics.draw(self.circleMesh,self.x,self.y,0,self.size/2,self.size/2)
    end
end

function Bullet:collision(cols)
    for _,col in ipairs(cols) do
        local other = col.other
        if self.canbeTarget[other.tag] and self.parent.tag~= other.tag then
            self:destroy()
            other:damage(self.damage)
        end
    end
end

function Bullet:destroy(silence)
    collidable.destroy(self) 
    if not silence then
        Boom(self,self.w*self.size/8)
    end
end

return Bullet