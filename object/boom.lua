local base = require "object/base"
local boom = class("boom",base)
boom.size = 1
function boom:init(parent,size)
    base.init(self,parent.x,parent.y)
    self.scaleX = size or 1
    self.scaleY = size or 1
    self.anim = res.explosion:copy()
    self.anim.mode = 1
	self.anim.onEnd = function()
       self:destroy() 
    end
end

return boom