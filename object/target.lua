local base = require "object/base"
local target = class("target",base)
target.color = {255,0,0} 

function target:init(parent,aim,size,lasting,...)
    base.init(self,... )
    self.parent = parent
  	self.aim = aim
  	self.x = aim.x
  	self.y = aim.y
  	self.size = size or 20
  	self.lasting = lasting or 3
end

function target:update(dt)
    base.update(self,dt)
    self.lasting = self.lasting - dt
    if not self.parent or not self.aim or self.lasting<0 then 
    	self:destroy()
    end
end

function target:draw()
	love.graphics.setColor(self.color)
	love.graphics.circle("line", self.x,self.y,self.size)
	love.graphics.line(self.x - self.size*1.5, self.y , self.x + self.size*1.5 , self.y)
	love.graphics.line(self.x , self.y - self.size*1.5, self.x , self.y + self.size*1.5)
end

return target