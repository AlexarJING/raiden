local base = require "object/base"
local ui = class("ui")

local playerPosition = {
    x = 100,
    y = 760
}

local bossPosition = {
    x = 100,
    y = 40
}

function ui:init()
    game.ui = self
end

function ui:draw()
    local player = game.player
    local hp = player.hp<0  and 0 or player.hp
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(player.image,player.quad,playerPosition.x,playerPosition.y, - math.pi/2,
                2,2,8,8)
    love.graphics.setColor(50, 255, 50, 50)
    love.graphics.rectangle("fill", 150,760,200, 10)
    love.graphics.setColor(50, 255, 50, 255)
	love.graphics.rectangle("fill", 150, 760,200*(hp/player.hpMax), 10)
	love.graphics.setColor(255, 255, 255, 255)
    love.graphics.rectangle("line", 150,760,200, 10)
	
    local boss = game.boss
    if not boss then return end
    local hp = boss.hp<0  and 0 or boss.hp
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(boss.image,boss.quad,bossPosition.x,bossPosition.y, math.pi/2,
                2,2,8,8)
    love.graphics.setColor(50, 255, 50, 50)
    love.graphics.rectangle("fill", 150,30,200, 10)
    love.graphics.setColor(50, 255, 50, 255)
	love.graphics.rectangle("fill", 150, 30,200*(hp/boss.hpMax), 10)
	love.graphics.setColor(255, 255, 255, 255)
    love.graphics.rectangle("line", 150,30,200, 10)
end

return ui