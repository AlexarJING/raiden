local Game = class("game")
local Base = require "object/base"
local Collidable = require "object/collidable"
local Ship = require "object/ship"
local Player = require "object/player"
local level =  require "scr/level"
local Star = require "object/star"
local UI = require "scr/ui"
function Game:init()
    self.world = bump.newWorld(64)
	self.objects = {}
    self:initBg()
    self:initUI()
    level:load(1)
    Player(250,700)
end

function Game:initBg()
    for i = 1, 10 do
        Star(love.math.random(0,500),love.math.random(0,800))
    end
end

function Game:initUI()
    UI()
end

function Game:update(dt)
    level:update(dt)
    for i = #self.objects,1 ,-1 do
		local go = self.objects[i]
		if go.destroyed then 
			table.remove(self.objects,i) 
		else
            go:update(dt)
        end
	end
end


function Game:draw()
	for i,v in ipairs(self.objects) do
        v:draw()
	end
    game.ui:draw()
end

function Game:gameover()
    local title = "Game Over!"
    local message = "blablabla"
    local buttons = {"OK"}
    local pressedbutton = love.window.showMessageBox(title, message, buttons)
    if pressedbutton == 1 then
        love.event.quit()
    end
end

return Game