local res = {}
love.graphics.setDefaultFilter( "nearest","nearest" )
res.font = love.graphics.newImageFont("res/imagefont.png",
    " abcdefghijklmnopqrstuvwxyz" ..
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
    "123456789.,!?-+/():;%&`'*#=[]\"")

res.sheet = love.graphics.newImage("res/ships_t.png")

res.engineFire = {}
for i=1,4 do
    res.engineFire[i]= Animation:new(res.sheet,28,468+(i-1)*12,8,8,4,4,71,511,0.1,4)
end

res.debris={}
for i=1,2 do 
    res.debris[i]=Animation:new(res.sheet,134,468+(i-1)*12,8,8,4,4,177,487,0.1,4)
end

res.explosion = Animation:new(res.sheet,201,468,16,16,4,4,297,487,0.05,4)
res.explosion.mode = 1

res.stars={}

for i=1,6 do
    res.stars[i]=Animation:new(res.sheet,436,468+(i-1)*20,16,16,4,4,530,487+(i-1)*20,0.1) 
end

res.missile=love.graphics.newQuad(26, 428, 16, 16, 999, 619)

local color={"blue","green","red","yellow","purple"}
res.ships={}
for i,v in ipairs(color) do
    local k=0
    res.ships[v]={}
    for y=1,19 do
        for x=1,5 do
            k=k+1
            res.ships[v][k]=love.graphics.newQuad((i-1)*110+28+(x-1)*20, 42+(y-1)*20, 16, 16, 999, 619)
        end
    end
end



return res

