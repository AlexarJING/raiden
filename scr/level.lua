local level = {}
local Enemy = require "object/enemy"

local Enemies = {}
for _,file in ipairs(love.filesystem.getDirectoryItems("object/enemies")) do
    local name = file:sub(1,-5)
    Enemies[tonumber(name)]=require("object/enemies/"..name)
end

local Bosses = {}
for _,file in ipairs(love.filesystem.getDirectoryItems("object/bosses")) do
    local name = file:sub(1,-5)
    Bosses[tonumber(name)]=require("object/bosses/"..name)
end

local testLevel = {}


local action = {
            t = 100,
            type = 1,
            x = 250,
            y = -50,
            rot = math.pi,
            isBoss = true
        } 
table.insert(testLevel,action)

for i = 1, 90 do
   local action = {
        t = i,
        type = love.math.random(1,3),
        x = love.math.random(50,450),
        y = -50,
        rot = math.pi
    } 
    
    table.insert(testLevel,action)
end

level.stack = {
    testLevel
}
function level:load(index)
    self.index = index
    self.t = 0
    self.track = self.stack[index]
end

function level:update(dt)
    self.t = self.t + dt
    for i = #self.track,1,-1 do
       local action = self.track[i]
        if action.t<self.t then
            if action.isBoss then
                Bosses[self.index](action.x,action.y,action.rot)
            else
                Enemies[action.type](action.x,action.y,action.rot)
            end
            table.remove(self.track,i)
        end
    end
    
end

return level